<?php

namespace Shopping\Domain\Service\ShoppingList;

use Shopping\Domain\Model\ShoppingList\NewShoppingRequest;
use Shopping\Domain\Model\ShoppingList\ShoppingList;
use Shopping\Domain\Model\ShoppingList\ShoppingRepository;

class NewShoppingListService
{
    /**
     * @var ShoppingRepository
     */
    private $repository;

    public function __construct(ShoppingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(NewShoppingRequest $shoppingRequest)
    {
        $shoppingList = new ShoppingList($shoppingRequest->getName(), []);
        $this->repository->addShoppingList($shoppingRequest->getToken(), $shoppingList);

    }
}