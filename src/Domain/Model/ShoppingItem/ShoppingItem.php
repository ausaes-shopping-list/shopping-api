<?php

namespace Shopping\Domain\Model\ShoppingItem;

use JsonSerializable;

class ShoppingItem implements JsonSerializable
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $id;
    /**
     * @var float
     */
    private $amount;
    /**
     * @var string
     */
    private $amountType;
    /**
     * @var int
     */
    private $quantity;

    public function __construct(string $id, string $name, int $quantity, float $amount, string $amountType)
    {
        $this->id = $id;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->amount = $amount;
        $this->amountType = $amountType;
    }

    public static function createNew(string $name, int $quantity, float $amount, string $amountType)
    {
        return new self(uniqid(), $name, $quantity, $amount, $amountType);
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'amount' => $this->getAmount(),
            'amountType' => $this->getAmountType()
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getAmountType(): string
    {
        return $this->amountType;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }


}