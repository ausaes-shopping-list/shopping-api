<?php


namespace Shopping\Domain\Model\ShoppingList;


use Shopping\Domain\Model\ShoppingItem\ShoppingItem;

interface ShoppingRepository
{

    /**
     * @param string $token
     * @param ShoppingList $shoppingList
     */
    public function addShoppingList(string $token, ShoppingList $shoppingList);

    /**
     * @param string $token
     * @param ShoppingList $shoppingList
     * @param ShoppingItem $shoppingItem
     * @return void
     */
    public function addItemToShoppingList(string $token, ShoppingList $shoppingList, ShoppingItem $shoppingItem): void;

    /**
     * @param string $token
     * @param ShoppingList $shoppingList
     * @param ShoppingItem[] $shoppingItems
     * @return void
     */
    public function addItemsToShoppingList(string $token, ShoppingList $shoppingList, array $shoppingItems): void;

    /**
     * @param string $token
     * @return ShoppingList[]
     */
    public function getShoppingLists(string $token): array;
}