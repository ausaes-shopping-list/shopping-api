<?php


namespace Shopping\Domain\Model\ShoppingList;


use JsonSerializable;
use Shopping\Domain\Model\ShoppingItem\ShoppingItem;

class ShoppingList implements JsonSerializable
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var array|ShoppingItem[]
     */
    private $shoppingItems;
    /**
     * @var string
     */
    private $id;

    /**
     * ShoppingList constructor.
     * @param string $id
     * @param string $name
     * @param ShoppingItem[] $shoppingItems
     */
    public function __construct(
        string $id,
        string $name,
        array $shoppingItems
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->shoppingItems = $shoppingItems;
    }

    /**
     * @param string $name
     * @param ShoppingItem[] $shoppingItems
     * @return ShoppingList
     */
    public static function createNew(string $name, array $shoppingItems)
    {
        return new self(uniqid(), $name, $shoppingItems);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array|ShoppingItem[]
     */
    public function getShoppingItems()
    {
        return $this->shoppingItems;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'items' => $this->getShoppingItems()
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}