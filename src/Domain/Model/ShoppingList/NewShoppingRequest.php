<?php


namespace Shopping\Domain\Model\ShoppingList;


class NewShoppingRequest
{
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $name;

    public function __construct(string $token, string $name)
    {

        $this->token = $token;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


}