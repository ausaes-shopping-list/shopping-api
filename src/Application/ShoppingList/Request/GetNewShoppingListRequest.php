<?php

namespace Shopping\Application\ShoppingList\Request;


use Exception;
use Shopping\Domain\Model\ShoppingList\NewShoppingRequest;

class GetNewShoppingListRequest
{
    /**
     * @param array $data
     * @return NewShoppingRequest
     * @throws Exception
     */
    public function build(array $data): NewShoppingRequest
    {
        $this->checkData($data);
        return new NewShoppingRequest($data['token'], $data['name']);
    }

    /**
     * @param array $data
     * @throws Exception
     */
    private function checkData(array $data)
    {
        if (!isset($data['token'])) {
            throw new Exception('user token is required');
        }
        if (!isset($data['name'])) {
            throw new Exception('list name is required');
        }
    }
}