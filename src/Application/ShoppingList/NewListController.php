<?php

namespace Shopping\Application\ShoppingList;

use Exception;
use Shopping\Application\ShoppingList\Request\GetNewShoppingListRequest;
use Shopping\Domain\Service\ShoppingList\NewShoppingListService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NewListController
{

    /**
     * @var GetNewShoppingListRequest
     */
    private $shoppingListRequest;
    /**
     * @var NewShoppingListService
     */
    private $service;

    public function __construct(GetNewShoppingListRequest $shoppingListRequest, NewShoppingListService $service)
    {
        $this->shoppingListRequest = $shoppingListRequest;
        $this->service = $service;
    }

    public function doAction(Request $request)
    {
        try {
            $params = [
                'token' => $request->headers->get('token'),
                'name' => $request->request->get('name')
            ];
            $shoppingRequest = $this->shoppingListRequest->build($params);
            $this->service->execute($shoppingRequest);
        } catch (Exception $e) {
            return new JsonResponse(json_encode(['error' => $e->getMessage()]), 500, [], true);
        }


        return new JsonResponse('{}', 201, [], true);
    }
}