<?php


namespace Shopping\Infrastructure\User\Mongo;


use Exception;
use MongoDB\Client;
use MongoDB\Collection;
use Shopping\Domain\Model\ShoppingItem\ShoppingItem;
use Shopping\Domain\Model\ShoppingList\ShoppingList;
use Shopping\Domain\Model\ShoppingList\ShoppingRepository;

class MongoShoppingRepository implements ShoppingRepository
{

    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $databaseName;

    /**
     * @var Collection
     */
    private $usersCollection;

    /**
     * @var Collection
     */
    private $itemsCollection;

    private CONST USERS_COLLECTION = 'users';
    private const ITEMS_COLLECTION = 'items';


    public function __construct(string $schema, string $databaseName)
    {
        $this->client = new Client($schema);
        $this->databaseName = $databaseName;
        $this->usersCollection = $this->client->{$this->databaseName}->{self::USERS_COLLECTION};
        $this->itemsCollection = $this->client->{$this->databaseName}->{self::ITEMS_COLLECTION};
    }

    /**
     * @param string $token
     * @param ShoppingList $shoppingList
     * @throws Exception
     */
    public function addShoppingList(string $token, ShoppingList $shoppingList)
    {
        $this->checkUserByToken($token);
        $this->usersCollection->updateOne(
            [
                'token' => $token
            ],
            [
                '$push' => [
                    'lists' => json_decode(json_encode($shoppingList))
                ]
            ]
        );
    }

    /**
     * @param string $token
     * @return ShoppingList[]
     * @throws Exception
     */
    public function getShoppingLists(string $token): array
    {
        $this->checkUserByToken($token);
        return [];
    }

    /**
     * @param string $token
     * @param ShoppingList $shoppingList
     * @param ShoppingItem $shoppingItem
     * @return void
     * @throws Exception
     */
    public function addItemToShoppingList(string $token, ShoppingList $shoppingList, ShoppingItem $shoppingItem): void
    {
        $this->checkUserByToken($token);
        $this->addItemToCollection($shoppingItem);
        $this->usersCollection->updateOne(
            [
                'token' => $token,
            ],
            [
                '$push' => [
                    'lists.$[list].items' => json_decode(json_encode($shoppingItem)),

                ],
            ],
            [
                'arrayFilters' => [
                    [
                        'list.id' => (int)$shoppingList->getId()]
                ]
            ]

        );
    }

    /**
     * @param string $token
     * @param ShoppingList $shoppingList
     * @param ShoppingItem[] $shoppingItems
     * @return void
     * @throws Exception
     */
    public function addItemsToShoppingList(string $token, ShoppingList $shoppingList, array $shoppingItems): void
    {
        $this->checkUserByToken($token);
        $this->usersCollection->updateOne(
            ['token' => $token],
            [
                '$push' => [
                    'lists.$[list].items' => [
                        '$each' => json_decode(json_encode($shoppingItems))
                    ],

                ]
            ],
            [
                'arrayFilters' => [
                    [
                        'list.id' => (int)$shoppingList->getId()]
                ]
            ]
        );
    }

    /**
     * @param string $token
     * @return void
     * @throws Exception
     */
    private function checkUserByToken(string $token)
    {
        $item = $this->usersCollection->findOne(['token' => $token]);
        if (empty($item)) {
            throw new Exception('User not found');
        }
    }

    private function addItemToCollection(ShoppingItem $item)
    {
        $this->itemsCollection->updateOne(
            [
                'name' => $item->getName()
            ],
            [
                '$set' => json_decode(json_encode($item))
            ],
            [
                'upsert' => true
            ]
        );
    }
}